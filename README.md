# Find a Dish

## System dependencies

The application was developed using the following tools. Other versions might work, but weren't tested:
- OpenJDK 11
- PostgreSQL 14.3
- ElasticSeach 8.2.2 (optional)
  - ElasticSearch also depends on a working JRE instalation
- Docker (recommended)

## API Documentation

The project uses Swagger to document its endpoints, you can access the Swagger UI by running the application and navigating to: http://localhost:8080/swagger-ui/index.html

## Usage

You can run the application using docker-compose by using the following command:

```sh
$ dbin/mvn spring-boot:run
```

or run locally using:

```sh
$ ./mvnw spring-boot:run
```

Some other util commands:

```sh
$ dbin/build    # rebuild the docker image (always run after changing the Dockefile)
$ dbin/clean    # remove the containers (preserving volume data)  
$ dbin/dispose  # remove all containers and volumes (WARNING: this will remove all database and cache data)
$ dbin/serve    # starts the app in background
$ dbin/db/start # start the database
$ dbin/status   # list all running services
$ dbin/stop     # stop the app and database
```

### Testing

To run the tests directly from the container, use the following command:

```sh
$ dbin/mvn test
```

or run locally using:

```sh
$ ./mvnw test
```

### Debugging

By default, the container will expose the debugger on port `8000`, so you can hook your IDE's remote debugger to it.

Remember that you can reclaim disk space by removing the containers and volumes when you are done using the application.