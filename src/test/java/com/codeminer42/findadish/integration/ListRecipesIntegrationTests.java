package com.codeminer42.findadish.integration;

import com.codeminer42.findadish.integration.base.BaseIntegrationTests;
import com.codeminer42.findadish.interfaces.http.RecipeRoute;
import com.codeminer42.findadish.interfaces.http.data.RecipeListItemResponse;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@SpringBootTest
public class ListRecipesIntegrationTests extends BaseIntegrationTests {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    Flyway flyway;
    @Autowired
    private WebApplicationContext context;
    private MockMvc mockMvc;

    @BeforeEach
    void before() {
        flyway.clean();
        flyway.migrate();

        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    public void shouldListAllRecipes() throws Exception {
        loadSeeds("recipes", "ingredients", "steps");

        var request = get(RecipeRoute.Path.recipeList());

        var response = makeRequest(mockMvc, request, RecipeListItemResponse.class);
        var responseStatus = response.getStatusCode();
        var responseBody = response.getBody();

        Assertions.assertEquals(200, responseStatus);
        Assertions.assertFalse(responseBody.getData().isEmpty());
    }
}
