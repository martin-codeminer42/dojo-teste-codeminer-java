package com.codeminer42.findadish.integration.base;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Objects;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@ActiveProfiles("test")
public class BaseIntegrationTests {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public <T> ResponseWrapper<T> makeRequest(MockMvc mockMvc, MockHttpServletRequestBuilder request, Class<T> javaClass) throws Exception {
        var mvcResult = mockMvc.perform(request).andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON)).andReturn();

        var statusCode = mvcResult.getResponse().getStatus();
        var returnedJson = mvcResult.getResponse().getContentAsString();
        var returnedObject = new ObjectMapper().readValue(returnedJson, javaClass);

        return new ResponseWrapper<>(statusCode, returnedObject);
    }

    public void loadSeeds(String... seeds) {
        Arrays.stream(seeds).forEach(seed -> {
            try {
                var statement = getStatement(seed);
                jdbcTemplate.execute(statement);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private String getStatement(String seed) throws IOException {
        return IOUtils.toString(
                Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("seeds/" + seed + ".sql")),
                StandardCharsets.UTF_8
        );
    }
}
