package com.codeminer42.findadish.integration;

import com.codeminer42.findadish.domain.RecipeRepository;
import com.codeminer42.findadish.integration.base.BaseIntegrationTests;
import com.codeminer42.findadish.interfaces.http.RecipeRoute;
import com.codeminer42.findadish.interfaces.http.data.CreateRecipeRequest;
import com.codeminer42.findadish.interfaces.http.data.RecipeResponse;
import com.codeminer42.findadish.interfaces.http.error.ValidationError;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;


@SpringBootTest
public class CreateRecipeIntegrationTests extends BaseIntegrationTests {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    Flyway flyway;
    @Autowired
    private WebApplicationContext context;
    @Autowired
    private RecipeRepository recipeRepository;
    private MockMvc mockMvc;

    @BeforeEach
    void before() {
        flyway.clean();
        flyway.migrate();

        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    @Transactional
    public void shouldCreateARecipe() throws Exception {
        var recipe = buildRecipe();
        var json = new ObjectMapper().writeValueAsString(recipe);
        var request = post(RecipeRoute.Path.createRecipe())
                .content(json)
                .contentType(MediaType.APPLICATION_JSON);

        var response = makeRequest(mockMvc, request, RecipeResponse.class);
        var responseStatus = response.getStatusCode();
        var responseBody = response.getBody();

        Assertions.assertEquals(201, responseStatus);
        Assertions.assertNotNull(responseBody.getData().getId());
        Assertions.assertFalse(responseBody.getData().getIngredients().isEmpty());
        Assertions.assertFalse(responseBody.getData().getSteps().isEmpty());

        var persistedRecipe = recipeRepository.findById(responseBody.getData().getId());

        Assertions.assertTrue(persistedRecipe.isPresent());
        Assertions.assertEquals(recipe.getName(), persistedRecipe.get().getName());
        Assertions.assertEquals(recipe.getPreparationTime(), persistedRecipe.get().getPreparationTime());
        Assertions.assertEquals(recipe.getSteps().size(), persistedRecipe.get().getSteps().size());
        Assertions.assertEquals(recipe.getIngredients().size(), persistedRecipe.get().getIngredients().size());
    }

    @Test
    public void shouldThrow422WhenMissingRequiredFields() throws Exception {
        var recipe = new CreateRecipeRequest();

        recipe.setName("");
        recipe.setPreparationTime(null);
        recipe.setSteps(List.of());
        recipe.setIngredients(List.of());

        var json = new ObjectMapper().writeValueAsString(recipe);
        var request = post(RecipeRoute.Path.createRecipe())
                .content(json)
                .contentType(MediaType.APPLICATION_JSON);

        var response = makeRequest(mockMvc, request, ValidationError.class);
        var responseStatus = response.getStatusCode();
        var responseBody = response.getBody();

        Assertions.assertEquals(422, responseStatus);

        assertValidationMessageIsPresent(responseBody, "Name can't be empty");
        assertValidationMessageIsPresent(responseBody, "Preparation time must be provided");
        assertValidationMessageIsPresent(responseBody, "At least one ingredient should be present");
        assertValidationMessageIsPresent(responseBody, "At least one step should be present");
    }

    @Test
    public void shouldThrow422WhenPassingInvalidProps() throws Exception {
        var recipe = buildRecipe();

        recipe.setPreparationTime(-1);
        recipe.getIngredients().get(0).setAmount(BigDecimal.ONE.negate());
        recipe.getIngredients().get(0).setUnit("long unit name");

        var nullIngredient = new CreateRecipeRequest.CreateRecipeIngredientRequestData();

        nullIngredient.setAmount(null);
        nullIngredient.setName("");
        nullIngredient.setUnit(null);

        recipe.getIngredients().add(nullIngredient);

        recipe.getSteps().get(0).setPosition(-1);

        var nullStep = new CreateRecipeRequest.CreateRecipeStepRequestData();

        nullStep.setPosition(null);
        nullStep.setDescription("");

        recipe.getSteps().add(nullStep);

        var json = new ObjectMapper().writeValueAsString(recipe);
        var request = post(RecipeRoute.Path.createRecipe())
                .content(json)
                .contentType(MediaType.APPLICATION_JSON);

        var response = makeRequest(mockMvc, request, ValidationError.class);
        var responseStatus = response.getStatusCode();
        var responseBody = response.getBody();

        Assertions.assertEquals(422, responseStatus);
        assertValidationMessageIsPresent(responseBody, "Preparation time must be at least zero");
        assertValidationMessageIsPresent(responseBody, "Ingredient name can't be empty");
        assertValidationMessageIsPresent(responseBody, "Ingredient amount must be provided");
        assertValidationMessageIsPresent(responseBody, "Ingredient amount must higher than zero");
        assertValidationMessageIsPresent(responseBody, "Ingredient unit can't be empty");
        assertValidationMessageIsPresent(responseBody, "Ingredient unit should have less than 5 characters");
        assertValidationMessageIsPresent(responseBody, "Step position should be at least zero");
        assertValidationMessageIsPresent(responseBody, "Step description can't be empty");
    }

    private CreateRecipeRequest buildRecipe() {
        final var ingredientData = new CreateRecipeRequest.CreateRecipeIngredientRequestData();
        ingredientData.setName("rice");
        ingredientData.setAmount(BigDecimal.ONE);
        ingredientData.setUnit("kg");

        final var stepData = new CreateRecipeRequest.CreateRecipeStepRequestData();
        stepData.setPosition(0);
        stepData.setDescription("cook rice");

        CreateRecipeRequest createRecipe = new CreateRecipeRequest();
        createRecipe.setName("My new recipe");
        createRecipe.setPreparationTime(20);
        createRecipe.setIngredients(new ArrayList<>(List.of(ingredientData)));
        createRecipe.setSteps(new ArrayList<>(List.of(stepData)));

        return createRecipe;
    }

    private void assertValidationMessageIsPresent(final ValidationError validationError, final String message) {
        Assertions.assertTrue(validationError.getDetails()
                .stream()
                .anyMatch(it -> it.equals(message)));
    }
}
