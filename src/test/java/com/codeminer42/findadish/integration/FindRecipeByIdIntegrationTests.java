package com.codeminer42.findadish.integration;

import com.codeminer42.findadish.integration.base.BaseIntegrationTests;
import com.codeminer42.findadish.interfaces.http.RecipeRoute;
import com.codeminer42.findadish.interfaces.http.data.RecipeResponse;
import com.codeminer42.findadish.interfaces.http.error.ApiError;
import com.codeminer42.findadish.interfaces.http.error.BadRequestError;
import com.codeminer42.findadish.interfaces.http.error.NotFoundError;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@SpringBootTest
public class FindRecipeByIdIntegrationTests extends BaseIntegrationTests {

    private final static UUID RECIPE_ID = UUID.fromString("bdccb30f-4297-497f-87fc-40d8c757b9ce");

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    Flyway flyway;
    @Autowired
    private WebApplicationContext context;
    private MockMvc mockMvc;

    @BeforeEach
    void before() {
        flyway.clean();
        flyway.migrate();

        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    public void shouldListAllRecipes() throws Exception {
        loadSeeds("recipes", "ingredients", "steps");

        var request = get(RecipeRoute.Path.recipe(RECIPE_ID));

        var response = makeRequest(mockMvc, request, RecipeResponse.class);
        var responseStatus = response.getStatusCode();
        var responseBody = response.getBody();

        Assertions.assertEquals(200, responseStatus);
        Assertions.assertEquals(responseBody.getData().getId(), RECIPE_ID);
        Assertions.assertFalse(responseBody.getData().getIngredients().isEmpty());
        Assertions.assertFalse(responseBody.getData().getSteps().isEmpty());
    }

    @Test
    public void shouldReturn404WhenNotFound() throws Exception {
        var request = get(RecipeRoute.Path.recipe(UUID.randomUUID()));

        var response = makeRequest(mockMvc, request, ApiError.class);
        var responseStatus = response.getStatusCode();
        var responseBody = response.getBody();

        Assertions.assertEquals(404, responseStatus);
        Assertions.assertEquals(responseBody.getMessage(), NotFoundError.MESSAGE);
    }

    @Test
    public void shouldReturn400WhenUsingInvalidID() throws Exception {
        var request = get(RecipeRoute.Path.recipe(UUID.randomUUID()) + "invalid-id-tail");

        var response = makeRequest(mockMvc, request, BadRequestError.class);
        var responseStatus = response.getStatusCode();
        var responseBody = response.getBody();

        Assertions.assertEquals(400, responseStatus);
        Assertions.assertEquals(responseBody.getDetails().get(0), "Invalid uuid");
    }
}
