insert into ingredient (id, name, amount, unit, recipe_id)
values ('282b193c-5faa-4545-8e53-b2fb2d0ca2d9', 'rice', 1, 'kg', 'bdccb30f-4297-497f-87fc-40d8c757b9ce'),
       ('00f1c8ed-783e-495b-ae48-f9322c63e2c6', 'beans', 1, 'kg', 'bdccb30f-4297-497f-87fc-40d8c757b9ce'),
       ('5bddacc3-9e69-4982-8a9d-55fc3d86e2b5', 'tomatoes', 1, 'un', 'bdccb30f-4297-497f-87fc-40d8c757b9ce'),

       ('002072ca-bb97-47a1-9b10-e26a56261698', 'beans', 1, 'kg', 'c83b0a1d-1e38-4773-b9b0-411a11ae6f6b'),
       ('4378e5a4-2cf2-48e0-b433-77d686a38f30', 'rice', 1, 'kg', 'c83b0a1d-1e38-4773-b9b0-411a11ae6f6b'),
       ('4b5f8d8c-0eac-4ef5-b01e-f7865432eaea', 'tomatoes', 1, 'un', 'c83b0a1d-1e38-4773-b9b0-411a11ae6f6b'),

       ('56ecf888-eb23-4387-8a13-02341f55c491', 'tomatoes', 1, 'un', 'c4b538b7-5e1e-4604-8f55-fbb3155bfa82'),
       ('3dfc2849-59b9-40e6-8f5d-6db890fcefda', 'beans', 1, 'kg', 'c4b538b7-5e1e-4604-8f55-fbb3155bfa82'),
       ('f2ad7c71-e864-4299-9f74-96c523d17569', 'rice', 1, 'kg', 'c4b538b7-5e1e-4604-8f55-fbb3155bfa82');