insert into step (id, description, position, recipe_id)
values ('cb2c0aa6-25ca-4856-b524-58e99bf929b1', 'put rice', 0, 'bdccb30f-4297-497f-87fc-40d8c757b9ce'),
       ('7d1cd5d3-ebba-4dd6-971e-982a53b4ea2b', 'put beans', 1, 'bdccb30f-4297-497f-87fc-40d8c757b9ce'),
       ('7c83de1f-2bd7-400f-a4e9-8a1071f3828f', 'put tomatoes', 2, 'bdccb30f-4297-497f-87fc-40d8c757b9ce'),

       ('5d155983-a922-4f5d-a498-a2a596ec6052', 'put beans', 0, 'c83b0a1d-1e38-4773-b9b0-411a11ae6f6b'),
       ('ea968791-8e8c-49d0-9484-c63e602d3a07', 'put rice', 1, 'c83b0a1d-1e38-4773-b9b0-411a11ae6f6b'),
       ('2c6b8010-9b97-4961-9ca3-a057963af1d1', 'put tomatoes', 2, 'c83b0a1d-1e38-4773-b9b0-411a11ae6f6b'),

       ('8087dffb-5eeb-4928-9eb5-95c01b2ca5ee', 'put tomatoes', 0, 'c4b538b7-5e1e-4604-8f55-fbb3155bfa82'),
       ('6ed8a231-ed83-4b3b-9dcc-7944b4dbee9d', 'put beans', 1, 'c4b538b7-5e1e-4604-8f55-fbb3155bfa82'),
       ('8c72d8e5-b3c7-4e95-8b1e-af95e8472324', 'put rice', 2, 'c4b538b7-5e1e-4604-8f55-fbb3155bfa82');
