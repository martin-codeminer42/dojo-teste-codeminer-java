create table recipe
(
    id               uuid primary key not null,
    name             varchar(255)     not null,
    preparation_time int              not null
);

create table step
(
    id          uuid primary key not null,
    description varchar(255)     not null,
    position    int              not null,
    recipe_id   uuid             not null references recipe (id)
);

create table ingredient
(
    id        uuid primary key not null,
    name      varchar(255)     not null,
    amount    decimal(6, 2)    not null,
    unit      varchar(5)       not null,
    recipe_id uuid             not null references recipe (id)
);