package com.codeminer42.findadish.infrastructure;

import com.codeminer42.findadish.application.dto.IngredientDTO;
import com.codeminer42.findadish.application.dto.RecipeDTO;
import com.codeminer42.findadish.application.dto.StepDTO;
import com.codeminer42.findadish.application.query.FindRecipeById;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public final class JdbcFindRecipeById implements FindRecipeById {
    private static final Logger LOG = LoggerFactory.getLogger(JdbcFindRecipeById.class);

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public Optional<RecipeDTO> query() {
        return Optional.empty();
    }

    @Override
    public Optional<RecipeDTO> query(final UUID recipeId) {
        final var ingredients = getIngredients(recipeId);
        final var steps = getSteps(recipeId);

        final var recipe = getRecipes(recipeId, steps, ingredients);

        if (recipe == null) {
            return Optional.empty();
        }

        return Optional.of(recipe);
    }

    private RecipeDTO getRecipes(final UUID recipeId,
                                 final List<StepDTO> steps,
                                 final List<IngredientDTO> ingredients) {

        final var sql = "SELECT * FROM recipe WHERE id = :recipeId";

        final var builder = RecipeDTO.builder()
                .id(recipeId)
                .steps(steps)
                .ingredients(ingredients);

        try {
            return jdbcTemplate.queryForObject(sql,
                    params(recipeId),
                    recipeRowMapper(builder)
            );
        } catch (EmptyResultDataAccessException e) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Recipe not found for id=" + recipeId.toString(), e);
            }

            return null;
        }
    }

    private List<StepDTO> getSteps(final UUID recipeId) {
        final var sql = "SELECT * FROM step WHERE recipe_id = :recipeId";

        return jdbcTemplate.query(sql, params(recipeId), stepRowMapper());
    }

    private List<IngredientDTO> getIngredients(final UUID recipeId) {
        final var sql = "SELECT * FROM ingredient WHERE recipe_id = :recipeId";

        return jdbcTemplate.query(sql, params(recipeId), ingredientRowMapper());
    }

    private RowMapper<RecipeDTO> recipeRowMapper(final RecipeDTO.RecipeDTOBuilder builder) {
        return (rs, rowNum) -> builder
                .name(rs.getString("name"))
                .preparationTime(rs.getInt("preparation_time"))
                .build();
    }

    private RowMapper<StepDTO> stepRowMapper() {
        return (rs, rowNum) -> new StepDTO(rs.getInt("position"), rs.getString("description"));
    }

    private RowMapper<IngredientDTO> ingredientRowMapper() {
        return (rs, rowNum) -> new IngredientDTO(rs.getString("name"),
                rs.getBigDecimal("amount"),
                rs.getString("unit"));
    }

    private SqlParameterSource params(final UUID recipeId) {
        return new MapSqlParameterSource().addValue("recipeId", recipeId);
    }
}
