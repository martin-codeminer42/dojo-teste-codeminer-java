package com.codeminer42.findadish.infrastructure;

import com.codeminer42.findadish.application.dto.RecipeListItemDTO;
import com.codeminer42.findadish.application.query.ListRecipes;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public final class JdbcListRecipes implements ListRecipes {
    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public List<RecipeListItemDTO> query() {
        final var sql = "SELECT * FROM recipe";

        return jdbcTemplate.query(sql, recipeListItemDTORowMapper());
    }

    private RowMapper<RecipeListItemDTO> recipeListItemDTORowMapper() {
        return (rs, rowNum) -> new RecipeListItemDTO(rs.getObject("id", UUID.class), rs.getString("name"));
    }
}
