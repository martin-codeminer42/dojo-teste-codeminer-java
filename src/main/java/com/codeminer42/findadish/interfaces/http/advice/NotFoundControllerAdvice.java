package com.codeminer42.findadish.interfaces.http.advice;

import com.codeminer42.findadish.interfaces.http.error.NotFoundError;
import com.codeminer42.findadish.lib.exception.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public final class NotFoundControllerAdvice {

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<NotFoundError> handleNotFoundExceptions(final NotFoundException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(NotFoundError.create());
    }
}
