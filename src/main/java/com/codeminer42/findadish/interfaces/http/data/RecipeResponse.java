package com.codeminer42.findadish.interfaces.http.data;

import com.codeminer42.findadish.application.dto.RecipeDTO;
import com.codeminer42.findadish.application.dto.StepDTO;
import io.swagger.annotations.ApiModel;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Getter
@ApiModel(value = "Recipe")
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public final class RecipeResponse {

    private RecipeResponseData data;

    public static RecipeResponse fromDTO(final RecipeDTO recipeDTO) {
        final var recipeData = new RecipeResponseData();

        recipeData.id = recipeDTO.getId();
        recipeData.name = recipeDTO.getName();
        recipeData.preparationTime = recipeDTO.getPreparationTime();

        recipeData.steps = recipeDTO.getSteps()
                .stream()
                .sorted(Comparator.comparingInt(StepDTO::getPosition))
                .map(step -> new RecipeStepResponseData(step.getPosition(), step.getDescription()))
                .collect(Collectors.toList());

        recipeData.ingredients = recipeDTO.getIngredients()
                .stream()
                .map(ingredient -> new RecipeIngredientResponseData(ingredient.getName(),
                        ingredient.getAmount(),
                        ingredient.getUnit()))
                .collect(Collectors.toList());

        return new RecipeResponse(recipeData);
    }

    @Getter
    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static final class RecipeResponseData {
        private UUID id;
        private String name;
        private Integer preparationTime;
        private List<RecipeStepResponseData> steps;
        private List<RecipeIngredientResponseData> ingredients;
    }

    @Getter
    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    @ApiModel(value = "Recipe.Ingredient")
    public static final class RecipeIngredientResponseData {
        private String name;
        private BigDecimal amount;
        private String unit;
    }

    @Getter
    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    @ApiModel(value = "Recipe.Step")
    public static final class RecipeStepResponseData {
        private Integer position;
        private String description;
    }
}
