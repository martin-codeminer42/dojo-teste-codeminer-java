package com.codeminer42.findadish.interfaces.http.error;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class BadRequestError extends ApiError {

    public static final String TYPE = "BadRequestError";
    public static final String MESSAGE = "Bad request error";

    private List<String> details;

    private BadRequestError(final List<String> messages) {
        super(TYPE, MESSAGE);
        this.details = messages;
    }

    public static BadRequestError create(final List<String> messages) {
        return new BadRequestError(messages);
    }
}
