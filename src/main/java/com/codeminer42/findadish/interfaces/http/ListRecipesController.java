package com.codeminer42.findadish.interfaces.http;

import com.codeminer42.findadish.application.query.ListRecipes;
import com.codeminer42.findadish.interfaces.http.data.RecipeListItemResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Api(value = "Recipes", tags = {"Recipes"})
public final class ListRecipesController {
    private final ListRecipes listRecipes;

    @RecipeRoute.List
    @ApiOperation(value = "List all recipes", tags = {"Recipes"})
    public ResponseEntity<RecipeListItemResponse> list() {
        final var recipeList = listRecipes.query();

        return ResponseEntity.ok()
                .body(RecipeListItemResponse.fromDTO(recipeList));
    }
}
