package com.codeminer42.findadish.interfaces.http;

import com.codeminer42.findadish.application.query.FindRecipeById;
import com.codeminer42.findadish.interfaces.http.data.RecipeResponse;
import com.codeminer42.findadish.lib.exception.NotFoundException;
import com.codeminer42.findadish.lib.validation.UUIDv4;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@Validated
@RestController
@RequiredArgsConstructor
@Api(value = "Recipes", tags = {"Recipes"})
public class FindRecipeByIdController {
    private final FindRecipeById findRecipeById;


    @RecipeRoute.Find
    @ApiOperation(value = "Find a recipe by Id", tags = {"Recipes"})
    public ResponseEntity<RecipeResponse> find(
            @ApiParam(type = "uuid") @PathVariable("recipeId") @UUIDv4 final String recipeId) {
        final var recipe = findRecipeById.query(UUID.fromString(recipeId))
                .orElseThrow(NotFoundException::new);

        return ResponseEntity.ok().body(RecipeResponse.fromDTO(recipe));
    }
}
