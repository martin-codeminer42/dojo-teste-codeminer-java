package com.codeminer42.findadish.interfaces.http.error;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ValidationError extends ApiError {

    public static final String TYPE = "ValidationError";
    public static final String MESSAGE = "Validation error";

    private List<String> details;

    private ValidationError(final List<String> validationMessages) {
        super(TYPE, MESSAGE);
        this.details = validationMessages;
    }

    public static ValidationError create(final List<String> validationMessages) {
        return new ValidationError(validationMessages);
    }
}
