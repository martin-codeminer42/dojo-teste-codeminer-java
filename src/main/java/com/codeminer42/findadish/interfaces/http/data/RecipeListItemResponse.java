package com.codeminer42.findadish.interfaces.http.data;

import com.codeminer42.findadish.application.dto.RecipeListItemDTO;
import io.swagger.annotations.ApiModel;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Getter
@ApiModel(value = "RecipeListItem")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class RecipeListItemResponse {
    private List<RecipeListItemResponseData> data;
    private Integer count;

    public static RecipeListItemResponse fromDTO(final List<RecipeListItemDTO> recipeListItemDTOList) {
        final var recipeListItems = recipeListItemDTOList.stream()
                .map(item -> new RecipeListItemResponseData(item.getId(), item.getName()))
                .collect(Collectors.toList());

        return new RecipeListItemResponse(recipeListItems, recipeListItems.size());
    }

    @Getter
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static final class RecipeListItemResponseData {
        private UUID id;
        private String name;
    }
}
