package com.codeminer42.findadish.interfaces.http.error;

import lombok.Getter;

@Getter
public final class NotFoundError extends ApiError {

    public static final String TYPE = "NotFoundError";
    public static final String MESSAGE = "Resource not found";

    private NotFoundError() {
        super(TYPE, MESSAGE);
    }

    public static NotFoundError create() {
        return new NotFoundError();
    }
}
