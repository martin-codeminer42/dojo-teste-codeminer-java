package com.codeminer42.findadish.interfaces.http;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.net.URI;
import java.util.UUID;

public @interface RecipeRoute {
    String BASE_URL = "/api/recipes";

    @Target({ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    @RequestMapping(value = RecipeRoute.BASE_URL, method = RequestMethod.POST)
    @interface Create {
    }

    @Target({ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    @RequestMapping(value = RecipeRoute.BASE_URL, method = RequestMethod.GET)
    @interface List {
    }

    @Target({ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    @RequestMapping(value = RecipeRoute.BASE_URL + "/{recipeId}", method = RequestMethod.GET)
    @interface Find {
    }

    final class Path {
        public static URI recipe(final UUID recipeId) {
            return URI.create(BASE_URL + "/" + recipeId.toString());
        }

        public static URI recipeList() {
            return URI.create(BASE_URL);
        }

        public static URI createRecipe() {
            return URI.create(BASE_URL);
        }
    }
}
