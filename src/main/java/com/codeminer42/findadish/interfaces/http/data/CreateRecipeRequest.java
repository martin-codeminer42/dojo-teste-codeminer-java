package com.codeminer42.findadish.interfaces.http.data;

import com.codeminer42.findadish.application.dto.CreateRecipeDTO;
import com.codeminer42.findadish.application.dto.IngredientDTO;
import com.codeminer42.findadish.application.dto.StepDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Data
@ApiModel(value = "CreateRecipe")
public final class CreateRecipeRequest {

    @NotBlank(message = "Name can't be empty")
    @ApiModelProperty(required = true)
    private String name;

    @NotNull(message = "Preparation time must be provided")
    @PositiveOrZero(message = "Preparation time must be at least zero")
    @ApiModelProperty(required = true)
    private Integer preparationTime;

    @Valid
    @NotEmpty(message = "At least one step should be present")
    @ApiModelProperty(required = true)
    private List<CreateRecipeStepRequestData> steps;

    @Valid
    @NotEmpty(message = "At least one ingredient should be present")
    @ApiModelProperty(required = true)
    private List<CreateRecipeIngredientRequestData> ingredients;

    public CreateRecipeDTO toDTO() {
        final var stepDTOList = steps.stream()
                .map(step -> new StepDTO(step.position, step.description))
                .collect(Collectors.toList());

        final var ingredientDTOList = ingredients.stream()
                .map(ingredient -> new IngredientDTO(ingredient.name, ingredient.amount, ingredient.unit))
                .collect(Collectors.toList());

        return CreateRecipeDTO.builder()
                .name(name)
                .preparationTime(preparationTime)
                .steps(stepDTOList)
                .ingredients(ingredientDTOList)
                .build();
    }

    @Data
    @ApiModel(value = "CreateRecipe.Ingredient")
    public static final class CreateRecipeIngredientRequestData {
        private static final int MAX_UNIT_LENGTH = 5;

        @NotBlank(message = "Ingredient name can't be empty")
        @ApiModelProperty(required = true)
        private String name;


        @NotNull(message = "Ingredient amount must be provided")
        @Positive(message = "Ingredient amount must higher than zero")
        @ApiModelProperty(required = true)
        private BigDecimal amount;

        @NotBlank(message = "Ingredient unit can't be empty")
        @Size(max = MAX_UNIT_LENGTH, message = "Ingredient unit should have less than 5 characters")
        @ApiModelProperty(required = true)
        private String unit;
    }

    @Data
    @ApiModel(value = "CreateRecipe.Step")
    public static final class CreateRecipeStepRequestData {

        @NotNull(message = "Step position must be provided")
        @PositiveOrZero(message = "Step position should be at least zero")
        @ApiModelProperty(required = true)
        private Integer position;

        @NotBlank(message = "Step description can't be empty")
        @ApiModelProperty(required = true)
        private String description;
    }
}
