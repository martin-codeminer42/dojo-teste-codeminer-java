package com.codeminer42.findadish.interfaces.http;

import com.codeminer42.findadish.application.usecases.CreateRecipe;
import com.codeminer42.findadish.interfaces.http.data.CreateRecipeRequest;
import com.codeminer42.findadish.interfaces.http.data.RecipeResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@Api(value = "Recipes", tags = {"Recipes"})
public final class CreateRecipeController {
    private final CreateRecipe createRecipe;

    @RecipeRoute.Create
    @ApiOperation(value = "Create a new recipe", tags = {"Recipes"})
    public ResponseEntity<RecipeResponse> create(@Valid @RequestBody final CreateRecipeRequest recipeData) {
        final var recipeDTO = createRecipe.execute(recipeData.toDTO());

        return ResponseEntity.created(RecipeRoute.Path.recipe(recipeDTO.getId()))
                .body(RecipeResponse.fromDTO(recipeDTO));
    }
}
