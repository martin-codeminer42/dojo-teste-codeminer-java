package com.codeminer42.findadish.config;

import com.codeminer42.findadish.application.dto.CreateRecipeDTO;
import com.codeminer42.findadish.application.dto.IngredientDTO;
import com.codeminer42.findadish.application.dto.RecipeDTO;
import com.codeminer42.findadish.application.dto.StepDTO;
import com.codeminer42.findadish.domain.Recipe;
import com.codeminer42.findadish.lib.Mapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.UUID;
import java.util.stream.Collectors;

@Configuration
public class MapperConfig {

    @Bean
    public Mapper<CreateRecipeDTO, Recipe> createRecipeDTOMapper() {
        return origin -> {
            final var recipe = new Recipe()
                    .withId(UUID.randomUUID())
                    .withName(origin.getName())
                    .withPreparationTime(origin.getPreparationTime());

            origin.getSteps().forEach(step -> recipe.addStep(step.getPosition(), step.getDescription()));

            origin.getIngredients()
                    .forEach(ingredient -> recipe.addIngredient(ingredient.getName(),
                            ingredient.getAmount(),
                            ingredient.getUnit()));

            return recipe;
        };
    }

    @Bean
    public Mapper<Recipe, RecipeDTO> recipeDTOMapper() {
        return origin -> {
            final var steps = origin.getSteps().stream()
                    .map(step -> new StepDTO(step.getPosition(), step.getDescription()))
                    .collect(Collectors.toList());

            final var ingredients = origin.getIngredients().stream()
                    .map(ingredient -> new IngredientDTO(ingredient.getName(),
                            ingredient.getAmount(),
                            ingredient.getUnit()))
                    .collect(Collectors.toList());

            return RecipeDTO.builder()
                    .id(origin.getId())
                    .name(origin.getName())
                    .preparationTime(origin.getPreparationTime())
                    .steps(steps)
                    .ingredients(ingredients)
                    .build();
        };
    }
}
