package com.codeminer42.findadish.domain;

import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@Builder
@AllArgsConstructor
@Table(name = "step")
@EqualsAndHashCode(of = {"id"})
public class Step {
    @Id
    private UUID id;
    private String description;
    private Integer position;

    @ManyToOne
    @JoinColumn(name = "recipe_id")
    private Recipe recipe;

    protected Step() {
    }

}
