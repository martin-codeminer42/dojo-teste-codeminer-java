package com.codeminer42.findadish.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.With;

@Data
@With
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "recipe")
@EqualsAndHashCode(of = {"id"})
public class Recipe {
    @Id
    private UUID id;
    private String name;
    private Integer preparationTime;

    @OneToMany(mappedBy = "recipe", cascade = CascadeType.ALL)
    private List<Step> steps = new ArrayList<>();

    @OneToMany(mappedBy = "recipe", cascade = CascadeType.ALL)
    private List<Ingredient> ingredients = new ArrayList<>();

    public final void addStep(final Integer position, final String description) {
        this.steps.add(
                Step.builder()
                        .id(UUID.randomUUID())
                        .position(position)
                        .description(description)
                        .recipe(this)
                        .build()
        );
    }

    public final void addIngredient(final String ingredient, final BigDecimal amount, final String unit) {
        this.ingredients.add(
                Ingredient.builder()
                        .id(UUID.randomUUID())
                        .name(ingredient)
                        .amount(amount)
                        .unit(unit)
                        .recipe(this)
                        .build()
        );
    }
}
