package com.codeminer42.findadish.domain;

import java.math.BigDecimal;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@Builder
@AllArgsConstructor
@Table(name = "ingredient")
@EqualsAndHashCode(of = {"id"})
public class Ingredient {
    @Id
    private UUID id;
    private String name;
    private BigDecimal amount;
    private String unit;

    @ManyToOne
    @JoinColumn(name = "recipe_id")
    private Recipe recipe;

    protected Ingredient() {
    }
}
