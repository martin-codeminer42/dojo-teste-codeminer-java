package com.codeminer42.findadish;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaAuditing
@SpringBootApplication
@EntityScan(basePackages = "com.codeminer42.findadish")
@EnableJpaRepositories(basePackages = "com.codeminer42.findadish")
public class FindADishApplication {

    public static void main(final String[] args) {
        SpringApplication.run(FindADishApplication.class, args);
    }

}
