package com.codeminer42.findadish.lib;

public interface Mapper<O, D> {
    D map(O origin);
}
