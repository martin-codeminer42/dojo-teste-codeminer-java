package com.codeminer42.findadish.lib.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import javax.validation.constraints.Pattern;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Retention(RUNTIME)
@ReportAsSingleViolation
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Constraint(validatedBy = {})
@Pattern(regexp = UUIDv4.REGEX_PATTERN)
public @interface UUIDv4 {
    String REGEX_PATTERN = "^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$";

    String message() default "Invalid uuid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
