package com.codeminer42.findadish.lib;

public interface Query<F, R> {
    R query();

    default R query(F filters) {
        return query();
    }
}
