package com.codeminer42.findadish.lib;

public interface ApplicationService<P, R> {
    R execute(P payload);
}
