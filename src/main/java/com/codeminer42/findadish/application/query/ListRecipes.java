package com.codeminer42.findadish.application.query;

import com.codeminer42.findadish.application.dto.RecipeListItemDTO;
import com.codeminer42.findadish.lib.Query;

import java.util.List;

public interface ListRecipes extends Query<Void, List<RecipeListItemDTO>> {
}
