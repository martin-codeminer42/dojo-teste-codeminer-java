package com.codeminer42.findadish.application.query;

import com.codeminer42.findadish.application.dto.RecipeDTO;
import com.codeminer42.findadish.lib.Query;

import java.util.Optional;
import java.util.UUID;

public interface FindRecipeById extends Query<UUID, Optional<RecipeDTO>> {
}
