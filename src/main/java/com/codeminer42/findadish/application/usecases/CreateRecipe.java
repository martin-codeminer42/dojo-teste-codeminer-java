package com.codeminer42.findadish.application.usecases;

import com.codeminer42.findadish.application.dto.CreateRecipeDTO;
import com.codeminer42.findadish.application.dto.RecipeDTO;
import com.codeminer42.findadish.domain.Recipe;
import com.codeminer42.findadish.domain.RecipeRepository;
import com.codeminer42.findadish.lib.ApplicationService;
import com.codeminer42.findadish.lib.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public final class CreateRecipe implements ApplicationService<CreateRecipeDTO, RecipeDTO> {

    private final RecipeRepository recipeRepository;
    private final Mapper<CreateRecipeDTO, Recipe> createRecipeDTOMapper;
    private final Mapper<Recipe, RecipeDTO> recipeDTOMapper;

    @Override
    public RecipeDTO execute(final CreateRecipeDTO payload) {
        final var recipe = createRecipeDTOMapper.map(payload);

        recipeRepository.save(recipe);

        return recipeDTOMapper.map(recipe);
    }
}
