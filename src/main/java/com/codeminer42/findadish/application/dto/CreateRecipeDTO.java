package com.codeminer42.findadish.application.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
@AllArgsConstructor
public final class CreateRecipeDTO {
    private final String name;
    private final Integer preparationTime;
    private final List<StepDTO> steps;
    private final List<IngredientDTO> ingredients;
}
