package com.codeminer42.findadish.application.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
@AllArgsConstructor
public final class IngredientDTO {
    private final String name;
    private final BigDecimal amount;
    private final String unit;
}
