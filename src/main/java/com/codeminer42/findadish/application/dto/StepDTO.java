package com.codeminer42.findadish.application.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public final class StepDTO {
    private final Integer position;
    private final String description;
}
