package com.codeminer42.findadish.application.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.List;
import java.util.UUID;

@Getter
@Builder
@AllArgsConstructor
public final class RecipeDTO {
    private final UUID id;
    private final String name;
    private final Integer preparationTime;
    private final List<StepDTO> steps;
    private final List<IngredientDTO> ingredients;
}
