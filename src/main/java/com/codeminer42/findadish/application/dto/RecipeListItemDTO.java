package com.codeminer42.findadish.application.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.UUID;

@Getter
@AllArgsConstructor
public final class RecipeListItemDTO {
    private final UUID id;
    private final String name;
}
